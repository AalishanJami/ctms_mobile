import * as React from 'react';
import { AsyncStorage, Button, Text, TextInput, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './screens/Auth/Login'
import BottomTabNavigator from './navigation/BottomTabNavigator';


const Stack = createStackNavigator();

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      userToken: ""
    }
    this._retrieveData
  }
  static navigationOptions = {
    header: null
  };

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('isLoign');
      if (value !== null) {
        // We have data!!
        console.log(value);
        this.setState({userToken: "true"})
      }
    } catch (error) {
      // Error retrieving data
    }
  };
  render(){
    return (

      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="PatientNav" component={BottomTabNavigator} />
        </Stack.Navigator>
      </NavigationContainer>

  );
  }

}
