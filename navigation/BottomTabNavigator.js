import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBarIcon from '../components/TabBarIcon';
import AntDesignIcon from '../components/AntDesignIcon';
import FontAwesomeBarIcon from '../components/FontAwesomeBarIcon';
import EntypoBarIcon from '../components/EntypoBarIcon';
import FoundationBarIcon from '../components/FoundationBarIcon';

import PatientDashboard from '../screens/Patient/PatientDashboard';
import PatientConsent from '../screens/Patient/PatientConsent';
import PatientEngagements from '../screens/Patient/PatientEngagements';
import PatientMessages from '../screens/Patient/PatientMessages';
import PatientTasks from '../screens/Patient/PatientTasks';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

export default function BottomTabNavigator({ navigation, route }) {

  navigation.setOptions({ headerTitle: getHeaderTitle(route) });

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME} tabBarOptions={{
      activeTintColor: '#0e4d92',
      inactiveTintColor: 'gray',
    }}>
      <BottomTab.Screen
        name="Dashboard"
        component={PatientDashboard}
        options={{
          title: 'Dashboard',
          tabBarIcon: ({ focused }) => <AntDesignIcon focused={focused} name="home" />,
        }}
      />
      <BottomTab.Screen
        name="Consent"
        component={PatientConsent}
        options={{
          title: 'Consent',
          tabBarIcon: ({ focused }) => <FoundationBarIcon focused={focused} name="clipboard-notes" />, 
        }}
      />
      <BottomTab.Screen
        name="Tasks"
        component={PatientTasks}
        options={{
          title: 'Tasks',
          tabBarIcon: ({ focused }) => <FontAwesomeBarIcon focused={focused} name="tasks" />,
        }}
      />
      <BottomTab.Screen
        name="Messages"
        component={PatientMessages}
        options={{
          title: 'Messages',
          tabBarIcon: ({ focused }) => <AntDesignIcon focused={focused} name="message1" />,
        }}
      />
      <BottomTab.Screen
        name="Engagements"
        component={PatientEngagements}
        options={{
          title: 'Engagements',
          tabBarIcon: ({ focused }) => <EntypoBarIcon focused={focused} name="network" />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'Home':
      return 'How to get started';
    case 'Links':
      return 'Links to learn more';
  }
}
