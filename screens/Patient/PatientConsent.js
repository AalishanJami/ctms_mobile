import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { Left, Right, Body } from "native-base";

export default class PatientConsent extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }
    render(){
        return(
            <View style={{marginTop: 24, flex: 1}}>
                <View style={{backgroundColor: "#0e4d92", height: 60, width:"100%"}}>
                    <Left />
                    <Body style={{flex: 1}} ><Text style={{color:"#fff", fontSize: 22}}>Consent</Text></Body>
                    <Right />
                </View>
                
            </View>
        );
    }
}
