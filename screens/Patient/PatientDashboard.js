import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { Left, Right, Body } from "native-base";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

export default class PatientDashboard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date: ""
        }
    }
    render(){
        return(
            <View style={{marginTop: 24, flex: 1}}>
                <View style={{backgroundColor: "#0e4d92", height: 60, width:"100%"}}>
                    <Left />
                    <Body style={{flex: 1}} ><Text style={{color:"#fff", fontSize: 22}}>Dashboard</Text></Body>
                    <Right />
                </View>

                <ScrollView style={{flex: 1,padding: 8}}>

                <Text style={{width:"100%", textAlign:"left", marginBottom: 8, fontSize: 18}}>Your Schedules</Text>
                <Calendar
                    // Initially visible month. Default = Date()
                    current={new Date()}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    // maxDate={this.state.androidDateend ? this.state.androidDateend : "2022-01-01"}
                    // Handler which gets executed on day press. Default = undefined
                    onDayPress={(day) => {
                        console.log(day);
                    
                    }}
                    // Handler which gets executed on day long press. Default = undefined
                    onDayLongPress={(day) => {console.log('selected day', day)}}
                    // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                    monthFormat={'MMM yyyy'}
                    // Handler which gets executed when visible month changes in calendar. Default = undefined
                    onMonthChange={(month) => {console.log('month changed', month)}}
                    // Hide month navigation arrows. Default = false
                    hideArrows={false}
                    // Replace default arrows with custom ones (direction can be 'left' or 'right')
                    // renderArrow={(direction) => (<Arrow />)}
                    // Do not show days of other months in month page. Default = false
                    hideExtraDays={true}
                    // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                    // day from another month that is visible in calendar page. Default = false
                    disableMonthChange={false}
                    // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                    firstDay={1}
                    // Hide day names. Default = false
                    hideDayNames={false}
                    // Show week numbers to the left. Default = false
                    showWeekNumbers={false}
                    // // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                    // onPressArrowLeft={substractMonth => substractMonth()}
                    // // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                    // onPressArrowRight={addMonth => addMonth()}
                    
                    markedDates={{
                        '2020-02-16': {selected: true, marked: true, selectedColor: 'blue'},
                        '2020-02-17': {marked: true},
                        '2020-02-18': {marked: true, dotColor: 'red', activeOpacity: 0},
                        '2020-02-19': {disabled: true, disableTouchEvent: true}
                    }}
                />
                </ScrollView>

            </View>
        );
    }
}
