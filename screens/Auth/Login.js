import React from "react";

import { View, Text, Image, Dimensions, TouchableOpacity, ToastAndroid, KeyboardAvoidingView } from "react-native";
import { Container, Header, Content, Item, Input, Icon } from "native-base";
import { EvilIcons } from '@expo/vector-icons';
import {AsyncStorage} from 'react-native';
import axios from "axios";
var width = Dimensions.get('window').width;
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      staff_id: "",
      password: "",
      passwordEmpty: false,
      staffEmpty: false,
      staffData:{}
    };
  }

  static navigationOptions = {
    header: null
  };

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('isLogin', "true");
      this.props.navigation.navigate('PatientNav')
    } catch (error) {
     console.log(error, "error saving data")
    }
  };

  render() {
    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled>
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        
        <Item style={{ marginLeft: 24, marginRight: 24 }}>
          <EvilIcons
            active name="envelope" size={22} color="#808080" />
          <Input placeholder="Staff ID" placeholderTextColor="#808080" style={{ fontSize: 16 }} value={this.state.staff_id} onChangeText={(text)=>{this.setState({staff_id: text})}}/>
        </Item>
        {this.state.staffEmpty ? <Text>Enter Email</Text> : null}
        <Item style={{ marginLeft: 24, marginRight: 24 }}>
          <EvilIcons
            active name="lock" size={22} color="#808080" />
          <Input placeholder="Password" secureTextEntry={true} placeholderTextColor="#808080" style={{ fontSize: 16 }} value={this.state.password} onChangeText={(text)=>{this.setState({password: text})}}/>
        </Item>
        {this.state.staffEmpty ? <Text>Enter Password</Text> : null}
        <TouchableOpacity onPress={this._storeData} style={{ width: width, height: 40, marginTop: 30 }}>
          <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 24, marginRight: 24, height: 40, alignItems: "center", justifyContent: "center" }}>
            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 18 }}>LOG IN</Text>
          </View>
        </TouchableOpacity>
      </View>
      </KeyboardAvoidingView>
    );
  }
}